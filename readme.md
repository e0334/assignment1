# E0 334 Assignment 1

In this assignment, you are tasked with implementing a preprocessing and classification pipeline for 3 datasets, namely:

1. The [Large Movie Review Dataset](http://ai.stanford.edu/~amaas/data/sentiment/) (IMDB).
2. The [Stanford Natural Language Inference Corpus](https://nlp.stanford.edu/projects/snli/) (SNLI).
3. [AG's News Topic Classification Dataset](https://github.com/mhjabreel/CharCNN/tree/master/data/ag_news_csv) (News).

For the purposes of this assignment, you are only allowed to use `Bag of Words` and `Bag of N-Grams`-based representations. You may make use of any classifier.

## Getting Started

1. Please start by forking this repository (tick the checkbox for `This is a private repository`). This should result in the creation of a new repository under your account.
2. Edit the three Python files to implement your solution. In case you are using Python 3, please update the Shebang (`#!`)  to reflect this.
3. Follow the submission instructions to share your solution with us. **Do not submit a Pull Request, unless you want to suggest a fix.**

## Submission Instructions

In your submission, create a `readme.md` file that includes the list of all packages **along with the specific versions that you make use of** that must be installed. Detail your results on the validation set, feature engineering and feature selection strategies and any ablation studies you might have performed separately in a `report.pdf` file. It should be possible to run your code on a fresh `virtualenv` after installing the specified packages without any other set-up. We will install the specified packages, and then run the three Python scripts to check your solution. Here is a sample list:

    numpy==1.14.1
	beautifulsoup4==4.6.0
    tensorflow==1.4.0

Please include your trained weights and load them automatically if training is time intensive (≥10 minutes).  

To submit your solution, go to `Settings > User and group access` and add `peteykun` with `Read` access to the `Users` section. We will automatically retrieve the latest submission via git.
